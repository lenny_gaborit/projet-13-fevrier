# PIERRE FEUILLE CISEAUX

![Texte alternatif](https://images-eu.ssl-images-amazon.com/images/I/51V-YSNt-iL.png)

```php
#1 Les importation
from random import randint
from typing import Dict
import sys
import os
```
Pour réaliser mon jeu j'ai d'abord importé toutes les fonctions qui vont m'être utile comme "randint" qui génère un nombre aléatoire, "DICT" qui indique que c'est un dictionnaire,"sys"qui me permet de stopper mon programme et "os" qui me permet de nettoyer le terminal 

```php
joueur1 = input("Entrez votre nom: ")
joueur2 = input("Entrez le nom du deuxième joueur: ")
```
Ensuite j'ai crée deux variables dans lesquelles on rentre le nom de chaque joueur grâce à "input".

```php 
#2 Les fonctions
def jeu():
    point_joueur1 = 0
    point_joueur2 = 0
```
J'ai crée une fonction nommée jeu qui ne prend en compte aucun argument. Puis j'ai initialisé deux compteurs pour les points de chaque joueur.

```php 
    continué = input("voulez vous joué ? OUI/NON  ")
    if continué not in ("OUI","NON"):
        continué = input("voulez vous joué ? OUI/NON")   
    if continué == "OUI":
        lancé = True
    else:
        sys.exit()
```
Cette partie du code demande au joueur s'il veut jouer, et stock sa réponse dans la variable "continué". Si "continué" contient "OUI" alors "lancé" est vrai ce qui va lancé la suite du programe sinon "sys.exit()" va arréter le programme

```php 
    while lancé:
        choix_joueur1 = ""
        choix_joueur2 = ""
        choix_possible = "PFC"
```
Ensuite "while lancé" signifie que la suite du programme va s'éxécuter si lancé est égale a TRUE donc si lancé est égale a TRUE 3 variables sont initialisés :  
-"choix_joueur1" et "choix_joueur2" qui s'initialisent vide et qui vont contenir les choix de chaque joueur  
-Et "choix_possible" qui est la variable qui contient les différents choix possibles des joueurs soit P soit F soit C 

```php 
        choix_joueur1 = input(joueur1+  " Pierre(P), Feuille(F), Ciseaux(C) : ") 
        while choix_joueur1 not in choix_possible:
            choix_joueur1 = input(joueur1+  " Pierre(P), Feuille(F), Ciseaux(C) : ") 
        os.system("cls")
```
Puis la variable "choix_joueur1" qui va contenir le choix du joueur 1 va être initialisé avec comme valeur soit P soit F soit C selon le choix du joueur 1.  
La partie "while choix_joueur1 not in choix_possible" va regarder si ce que le joueur a joué est bien dans la varaible choix_possible initialisé précedèmment avec les valeurs P, F ou C
autremment dit il va regarder si le joueur a bien joué soit F pour Feuille soit C pour Ciseaux soit P pour Pierre et si ce n'est pas le cas il va lui demandé de rejouer.  
"os.system("cls") la fonction os que j'ai importé au debut va effacer le terminal pour que le joueur 2 puisse jouer sans voir ce que le joueur 1 a joué.

```php
        choix_joueur2 = input(joueur2+  " Pierre(P), Feuille(F), Ciseaux(C) : ")
        while choix_joueur2 not in choix_possible:
            choix_joueur2 = input(joueur2+  " Pierre(P), Feuille(F), Ciseaux(C) : ") 
        os.system("cls")
```
Le programme va répéter les mêmes étapes pour le joueur 2

```php
        choix={"P":1,"F":2,"C":3,}     
        choix_joueur1 = choix[choix_joueur1]
        choix_joueur2 = choix[choix_joueur2]
```
Ensuite j'ai créé un dictionnaire choix qui a comme clé les choix possible et qui asocie a chaque choix un nombre.  
"choix_joueur1" va donc prendre la valeur numérique du dictionnaire qui est associé a la clé correspondant a ce que le joueur 1 a joué et pareil pour "choix_joueur2"

```php
        if choix_joueur1 - choix_joueur2 == 0:
            print("Dommage égaliter")
```
La condition "if" va regardé si choix_joueur1 mois le choix_joueur2 est égale a 0 ce qui voudrait dire que les deux joueur est joué la meme chose par exemple Pierre=1 donc si les deux joueur joue pierre 1-1=0.  
Donc le programme affiche Dommage égaliter.

```php
        if choix_joueur1 - choix_joueur2 == 2 or choix_joueur1 - choix_joueur2 ==-1:
            point_joueur2 +=1
            print("Bien jouer " + joueur2 + "\n" + joueur2 + " a " + str(point_joueur1) +" points" )
```
Même processus que pour l'égaliter le "if" va regarder si choix_joueur1 - choix_joueur2 est égale a 2 ou -1 et si c'est le cas il va ajouter 1 point au compteur de point du joueur 2 et afficher que le joueur 2 a gagné et son compteur de point.


```php
        if choix_joueur1 - choix_joueur2 == -2 or choix_joueur1 - choix_joueur2 ==1:
            point_joueur1 +=1
            print("Bien jouer " + joueur1 + "\n" + joueur1 + " a " + str(point_joueur1) +" points" )
```
Toujours pareil "if" regarde si choix_joueur - choix_joueur2 est égale a -2 ou 1.  
Et comme pour le joueur 1 il va ajouté 1 point a son compteur de point puis dire qu'il a gagné et son nombre de point.

```php
        continué = input("Voulez vous rejouer ? OUI/NON  ")
        if continué == "NON":
            lancé = False
```
Enfin le programme va demandé aux joueurs si ils veulent continuer a jouer et si ils ne veulent pas il va initialisé lancé a FALSE se qui va stopper le programme au lieu de le redémaré

```php
jeu()
```
Pour terminer cette partie du programme va appelé la fonction jeu ce qui permet de la lancer lors de l'éxecution du programme  
  
__TENTEZ VOTRE CHANCE !!__